public class MethodVoid {
    public static void main(String[] args) {
        System.out.println("Ini adalah Method void");

        // void artinya adalah hampa atau tidak memiliki nilai

        System.out.println(simpel());
        fungsiVoid("kembalian");

    } 
    // fungsi method tanpa kembalian
    private static void fungsiVoid(String input){
        System.out.println(input);

    }

        // fungsi method dengan kembalian
        // sehingga menggunaka return untuk 
        // mengembalikan hasilnya
        private static float simpel(){ 
            return 10.9f;
    }
}
