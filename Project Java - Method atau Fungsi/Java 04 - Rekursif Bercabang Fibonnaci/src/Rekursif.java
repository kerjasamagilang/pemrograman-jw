public class Rekursif {
    public static void main(String[] args) {
        System.out.println("Ini Rekursif Bercabang Fibonnaci");
        nilaiFibonnaci(5);
    }

    private static int nilaiFibonnaci(int n){
        System.out.println(n);
        if (n == 1 || n == 0){
            return 0;
        }
        return nilaiFibonnaci(n - 1) + nilaiFibonnaci(n - 2);
        
        
    }
}
