public class Metode1 {
    public static void main(String[] args) {
        System.out.println("Ini adalah Method Sederhana");

        int y,x;
        x = 10;
        y = hitung(x);
        System.out.println("x = " + x + ", y = " + y);


        x = 20;
        y = hitung(x);
        System.out.println("x = " + x + ", y = " + y);


        x = 30;
        y = hitung(x);
        System.out.println("x = " + x + ", y = " + y);

    }

    static int hitung(int input) {
        int hasil;
        hasil = (input + 2 ) * input;
        return hasil;
        
    }
}