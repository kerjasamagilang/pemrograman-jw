public class Metode {

    // ini adalah contoh Method
    static void ucapSalam(){
        System.out.println("Selamat Pagi");
        }

    public static void main(String[] args) {
        // Ini adalah Method sederhana

        /*
        Method/fungsi dapat memecah program menjadi sub-sub program,
        sehingga kita bisa membuat program lebih efisien.
        Penggunaan method/fungsi dapat mengurangi pengetikan kode yang berulang-ulang.
        */

        // Cara membuat fungsi
        
        // static TypeDataKembalian namaFungsi(){
        //      statemen atau kode fungsi

        // Penjelasan
        
        /*
            Kata kunci static, artinya kita membuat fungsi yang dapat dipanggil
            tanpa harus membuat instansiasi objek.

            TypeDataKembalian adalah tipe data dari nilai yang
            dikembalikan setelah fungsi dieksekusi.

            namaFungsi() adalah nama fungsinya. Biasanya ditulis dengan huruf kecil di awalnya.
            Lalu, kalau terdapat lebih dari satu suku kata, huruf awal di kata kedua ditulis kapital.
        */

        // Contoh ini dibuat dalam satu file
        
        
        ucapSalam(); // ketika method dipanggil
    }
}
