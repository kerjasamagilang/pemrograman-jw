// Membuat class sebagai template tanpa Constructor / polos
class TanpaContructor {
    String dataString;
    int dataInteger;
}

// class dengan Constructor 

class DenganConstructor{
    String nama;
    String alamat;
    int umur;

    // Membuat Contructor
    // Contructor ini dipanggil pertama kali ketika object dibuat
    // DenganConstructor() {
    //     System.out.println("Ini adalah Constructor");
    // }

    
    
    // Contructor dengan parameter
    DenganConstructor(String inputNama, String inputAlamat, int inputUmur) {
        nama = inputNama;
        alamat = inputAlamat;
        umur = inputUmur;

        System.out.println(nama);
        System.out.println(alamat);
        System.out.println(umur);
    }
}

public class Konstruksi {
    public static void main(String[] args) {


        // Membuat Object dengan Contructor
        DenganConstructor pakaiConstructor = new DenganConstructor("Gilang", "KDM", 20);
        System.out.println(pakaiConstructor.nama);


        // Object Tanpa Constructor

        // TanpaConstructor tanpa = new TanpaContructor();
        // tanpa.dataInteger = 10;
        // tanpa.dataString = "polos";

        // System.out.println(tanpa.dataInteger);

    }
    
}
