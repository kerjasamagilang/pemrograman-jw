class Buku{
    String judul;
    String penulis;

    Buku(String judul, String penulis) {
        this.judul = judul;
        this.penulis = penulis;
    }

    void display (){
        System.out.println("Buku " + this.judul);
        System.out.println("Penulis " + this.penulis);
    }
}

public class Reference{
    public static void main(String[] args) {
        Buku bukuPerpus = new Buku("Gilang", "Purnama");

        bukuPerpus.display();

        String addressBukuPerpus = Integer.toHexString(System.identityHashCode(bukuPerpus));
        System.out.println(addressBukuPerpus);
        
        // References object
        // References tidak menduplikasi object
        Buku bukuGilang = bukuPerpus;
        bukuGilang.display();
        String addressBukuGilang = Integer.toHexString(System.identityHashCode(bukuGilang));
        System.out.println(addressBukuGilang);
    }
}