class Vehicles{
    String pemilik;
    String kendaraan;
    int roda;
    Penumpang penumpang;

    Vehicles(String pemilik, String kendaraan, int roda){
        this.pemilik = pemilik;
        this.kendaraan = kendaraan;
        this.roda = roda;
        
    }

    void show(){
        System.out.println("Nama Pemilik\t\t: " + this.pemilik);
        System.out.println("Kendaraan\t\t: " + this.kendaraan);
        System.out.println("Jumlah Roda\t\t: " + this.roda);

    }

    void display(){
        
        System.out.println("Jumlah Penumpang\t: " + this.penumpang);
    }
}

class Penumpang{
    int orang;
    int ongkos;
    
    Penumpang(int orang, int ongkos){
        this.orang = orang;
        this.ongkos = ongkos;
    }

    void show(){
        System.out.println("Jumlah Penumpang\t: " + this.orang);
        System.out.println("Total Ongkos\t\t: " + this.ongkos * 24);

    }
}

public class Latihan {
    public static void main(String[] args) {
        
        // Membuat object Kendaraan sederhana
        Vehicles dataVehicles = new Vehicles("PT. Primajasa", "Bus", 6);

        // Membuat object Penumpang
        Penumpang dataPenumpang = new Penumpang(24, 25000);

        dataVehicles.show();
        dataPenumpang.show();
    }
}
