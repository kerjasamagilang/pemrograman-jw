// player
class Player{
    String nama;
    double kesehatan;

    // object member
    Senjata senjata;
    Pertahanan pertahanan;

    Player(String nama, double kesehatan){
        this.nama = nama;
        this.kesehatan = kesehatan;
    }

    void equipSenjata(Senjata senjata){
        this.senjata = senjata;
    }

    void equipPertahanan(Pertahanan Pertahanan){
        this.pertahanan = Pertahanan;
    }

    void display(){
        System.out.println("\nnama \t\t: " + this.nama);
        System.out.println("kesehatan \t: " + this.kesehatan + " hp");
        this.senjata.display();
        this.pertahanan.display();
    }
}

// senjata
class Senjata{
    double attackPower;
    String nama;

    Senjata(String nama, double attackPower){
        this.nama = nama;
        this.attackPower = attackPower;
    }

    void display(){
        System.out.println("senjata \t: " + this.nama + " , attack : " + this.attackPower);
    }
}

// Pertahanan
class Pertahanan{
    double defencePower;
    String nama;

    Pertahanan(String nama, double defencePower){
        this.nama = nama;
        this.defencePower = defencePower;
    }

    void display(){
        System.out.println("Pertahanan \t: " + this.nama + " , defence : " + this.defencePower);
    }
}

public class LatihanOOP {

    public static void main(String[] args) {
        
        // membuat object player
        Player player1 = new Player("Gilang",100);
        Player player2 = new Player("Purnama",50);

        // membuat object senjata
        Senjata pedang = new Senjata("pedang",15);
        Senjata ketapel = new Senjata("ketapel",1);

        // membuat object Pertahanan
        Pertahanan bajuBesi = new Pertahanan("baju besi",10);
        Pertahanan kaos = new Pertahanan("kaos",0);
        
        // player 1
        player1.equipSenjata(pedang);
        player1.equipPertahanan(bajuBesi);
        player1.display();

        // player 2
        player2.equipSenjata(ketapel);
        player2.equipPertahanan(kaos);
        player2.display();
    }
}
