class CekSuhu {
    // data Member
    String nama;
    String hari;
    float SuhuTubuh;

    // constructor
    CekSuhu(String nama, String hari, float SuhuTubuh){
        this.nama = nama;
        this.hari = hari;
        this.SuhuTubuh = SuhuTubuh;
    }

    // method tanpa return dan tanpa parameter
    // menempelkan method dengan object = tidak memakai static
    void show(){
        System.out.println("Nama : " + this.nama);
        System.out.println("Hari : " + this.hari);
        System.out.println("Suhu Tubuh : " + this.SuhuTubuh);
    }

    // method tanpa return tapi dengan parameter
    void setNama(String nama){
        this.nama = nama;
    }
    // method dengan return tapi tidak ada parameter
    String getNama(){
        return this.nama;
    }

    // method dengan return dan parameter
    String sayHi(String pesan){
        return pesan + "Hallo juga, nama saya " + this.nama;
    }
    void setHari(String hari, float SuhuTubuh){
        this.hari = hari;
        this.SuhuTubuh = SuhuTubuh;
    }
}

public class MethodOop {
    public static void main(String[] args) {
        CekSuhu anakKDM = new CekSuhu("Gilang", "Senin", 36.3f);
        // System.out.println(anakKDM.nama);
        // anakKDM.show();
        // anakKDM.setNama("Gilang Purnama");
        anakKDM.setHari("Selasa", 36.7f);
        anakKDM.show();
        // anakKDM.show();

        // System.out.println(anakKDM.getNama());

        // String pesan = anakKDM.sayHi("Hallo \n");
        // System.out.println(pesan);
    }
}
