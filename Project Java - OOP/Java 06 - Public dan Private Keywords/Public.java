class Anak{
    String nama; // default, dia akan bisa dibaca dan ditulis dari luar class
    public int exp; // public, dia akan bisa dibaca dan ditulis dari luar class

    private int level; // private, hanya bisa dibaca dan ditulis di dalam class saja

    Anak(String nama, int exp, int level){
        this.nama = nama;
        this.exp = exp;
        this.level = level;
    }

    // default modifier access
    void show(){
        System.out.println("Nama \t" + this.nama);
        System.out.println("exp \t" + this.exp);
        System.out.println("Level \t" + this.level); // Menulis di dalam class
    }
}

public class Public {
    public static void main(String[] args) {

        Anak k4 = new Anak("Gilang", 0, 7 );

        // DEFAULT
        System.out.println(k4.nama); // Membaca data
        k4.nama = "Purnama"; // Menulis Data
        System.out.println(k4.nama);
        
        // PUBLIC
        System.out.println(k4.exp); // Membaca data
        k4.exp = 100; // Menulis Data
        System.out.println(k4.exp);
        
        // PRIVATE (Tidak bisa diakses)
        // System.out.println(k4.level); // Membaca data
        // k4.level = 100; // Menulis Data
        // System.out.println(k4.level);

        k4.show();
    }
    
}
