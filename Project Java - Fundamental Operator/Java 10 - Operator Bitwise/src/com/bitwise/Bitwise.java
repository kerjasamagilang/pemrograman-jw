package com.bitwise;
public class Bitwise {
    public static void main(String[] args){
        System.out.println("Hello Bitwise");


        // Operator Bitwise >--> operator untuk melakukan operasi pada nilai bit
        byte a = 3;
        byte b, c;
        String a_bits, b_bits, c_bits;


        // Operator SHIFT LEFT #Geser ke kiri
        System.out.println("===== SHIFT LEFT");
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b = (byte)(a << 3); // menggeser nilai ke kiri sebanyak yang ditentukan
        b_bits = String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0');
        System.out.printf("%s = %d \n", b_bits, b);

        // Operator SHIFT RIGHT #Geser ke kanan
        System.out.println("===== SHIFT RIGHT");
        a = 24;
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b = (byte)(a >> 2); // menggeser nilai ke kanan sebanyak yang ditentukan
        b_bits = String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0');
        System.out.printf("%s = %d \n", b_bits, b, "\n");


        // Operator OR bitwise
        System.out.println("===== BITSIWE OR  | ");
        a = 24;
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b = 12; 
        b_bits = String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0');
        System.out.printf("%s = %d \n", b_bits, b);

        c = (byte)(a | b);
        c_bits = String.format("%8s", Integer.toBinaryString(c)).replace(' ', '0');
        System.out.printf("%s = %d \n", c_bits, c, "\n");

        // Operator AND bitwise
        System.out.println("===== BITSIWE AND  & ");
        a = 24;
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b = 12; 
        b_bits = String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0');
        System.out.printf("%s = %d \n", b_bits, b);

        c = (byte)(a & b);
        c_bits = String.format("%8s", Integer.toBinaryString(c)).replace(' ', '0');
        System.out.printf("%s = %d \n", c_bits, c, "\n");

        // Operator XOR bitwise
        System.out.println("===== BITSIWE XOR  ^ ");
        a = 24;
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b = 12; 
        b_bits = String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0');
        System.out.printf("%s = %d \n", b_bits, b);

        c = (byte)(a ^ b);
        c_bits = String.format("%8s", Integer.toBinaryString(c)).replace(' ', '0');
        System.out.printf("%s = %d \n", c_bits, c, "\n");

        // Operator NEGASI bitwise
        System.out.println("===== BITSIWE NEGASI ~ ");
        a = 24;
        b = (byte)(~a);
        a_bits = String.format("%8s", Integer.toBinaryString(a)).replace(' ', '0');
        System.out.printf("%s = %d \n", a_bits, a, "\n");
        b_bits = String.format("%8s", Integer.toBinaryString(b)).substring(24);
        System.out.printf("%s = %d \n", b_bits, b);
    }
}
