Ini bilangan BINER
    
        Biner adalah bilangan berbasis 2 yaitu 0 dan 1, contoh 01000010
        Perhitngan akan dilakukan dengan menggunakan perkalian kelipatan 2
        Perhitungan ini akan dikonversi ke dalam angka desimal

            ⁰¹²³⁴⁵⁶⁷⁸⁹
        2⁰ = 1
        2¹ = 2
        2² = 4
        2³ = 8
        2⁴ = 16
        2⁵ = 32
        2⁶ = 64
        2⁷ = 128
        Dimulai dari sebelah kanan
        01000010 =
                    0 * 2⁰ = 0
                    1 * 2¹ = 2   
                    0 * 2² = 0
                    0 * 2³ = 0
                    0 * 2⁴ = 0
                    0 * 2⁵ = 0
                    1 * 2⁶ = 64
                    0 * 2⁷ = 0
                    
                    0 + 2 + 0 + 0 + 0 + 0 + 64 + 0 = 66

        bilangan desimal dari angka biner ini 01000010 adalah 66

==================================================================================================
    Ini bilangan DESIMAL
    
    Desimal adalah bilangan basis 10
    0 1 2 3 4 5 6 7 8 9

    contoh 3 7 5
    
                5 * 1 (5 * 10⁰)
                7 * 10 (5 * 10¹)
                3 * 100 (3 * 10³)

    Perhitungan ini akan dikonversi ke dalam angka biner
    ANGKA BINER
        2⁰ = 1
        2¹ = 2
        2² = 4
        2³ = 8
        2⁴ = 16
        2⁵ = 32
        2⁶ = 64
        2⁷ = 128
        2⁸ = 256
        2⁹ = 512
    
    Desimal 135 = ..... biner
    
    128  64  16   8   4   2   1
    
    128 + 64 = 192 X ( karena sudah melebihi angka dari DESIMAL 135 )
    128 + 16 = 144 X ( karena sudah melebihi angka dari DESIMAL 135 )
    128 + 8 = 136 X ( karena sudah melebihi angka dari DESIMAL 135 )
    128 + 4 = 132 ✓ ( karena masih kecil dari angka DESIMAL 135 )
    132 + 2 = 134 ✓ ( karena masih kecil dari angka DESIMAL 135 )
    134 + 1 = 135 ✓ ( angka ditemukan 135 )
    
    setiap angka yang ditambah dan hasilnya melebihi angaka utama maka dianggap 0
    dan setiap angka yang ditambah dan hasilnya masih lebih kecil dari angka utama maka dianggap 1

    1 3 5
    128  64  32   16   8   4   2   1
    1    0   0    0    0   1   1   1
    
        Desimal 1 3 5 = 1 0 0 0 0 1 1 1
        Biner 1 0 0 0 1 1 1 = .... Desimal
        
        128         64            32         16        8          4          2      1
        
        128    +    0      +      0     +    0     +   0    +     4     +    2   +  1 = 1 3 5
    
==================================================================================================

    Aritmatika bilangan Biner dan Desimal

        Penjumlahan angka Desimal 777 + 135 = 912

        Bagaimana melakukan penjumlahan dalam format biner?
        001100001001 (777)
            10000111 (135)
        ___________________+


        teori dasar penjumlahan angka biner

            0 + 0 = 0
            0 + 1 = 1
            1 + 0 = 1 
            1 + 1 = 10 ( Angka satu dari 10 akan disimpan untuk penjumlahan berikutnya )

                      ¹ ¹ ¹ ¹ ( Angka Simpanan )
        0 0 1 1 0 0 0 0 1 0 0 1 (777)
                1 0 0 0 0 1 1 1 (135)
        _________________________+
            1 1 1 0 0 1 0 0 0 0
            

==================================================================================================

    Aritmatika Pengurangan bilangan Biner
    
    teori dasar pengurangan Biner
            0 - 0 = 0
            1 - 0 = 1
            0 - 1 = 1 ( Meminjam nilai selanjutnya )
            1 - 1 = 0

            777 - 135 = 642 (Desimal)
            
            
                            Meminjam Angka
                                ^
            0 0 1 1 0 0 0 0 1 0 0 1 (777)
                    1 0 0 0 0 1 1 1 (135)
            ________________________-              
            0 0 1 0 1 0 0 0 0 0 1 0 (642)


    ♦ Pada dasarnya, bilangan Biner tidak mengenal operasi pengurangan
        ♦ Processor tetap melakukan operasi penjumlahan untuk permintaan operasi pengurangan
        ♦ Setiap operasi pengurangan akan dijadikan operasi penjumlahan

    
    Contoh :
            60 - 135 = -75 (Desimal)
            
            60 + (-135) = -75 (Desimal)
        
        Variable Integer
        
        X = 135 (1000 0111)
        Y = -135 (?????????)
                 ? Bagaimana cara menemukannya
                
                    Dengan metode One`s comploment → Invers bits dan Two`s comploment
                    One`s comploment
                    Desimal : 1
                    Binary  : 0000 0001 
                            Melakukan invers bits adalah kita berubah setiap nilai 0 menjadi nilai 1
                            dan nilai 1 menjadi nilai 0
                            0000 0001 → 1111 1110

                    Untuk menemukan nilai Two`s comploment
                    Secara sederhana jika kita memiliki Desimal 1 dan nilai binary

                    Two`s comploment
                    Desimal = -1 
                    Binary  = 0000 0001 → 1111 1110 ( One`s comploment)
                                                    kita melakukan proses invers bits terlebih dahulu
                                                    untuk mendapatkan One`s comploment
                                                    dari hasil One`s comploment ditambahkan dengan angka 1
                                                    
                                                    1111 1110
                                                            1
                                                    _________+
                                                    1111 1111
                    
                    Bilangan 1111 1111 dari proses ini dinamakan Two`s comploment
                    dan secara tidak langgsung nilai itu juga adalah bilangan negatif
                    dari nilai Desimal yang baru saja kita proses.
==================================================================================================
    Aritmatika Perkalian dan pembagian bilangan Biner

    Proses Perkalian
    3 * 5 = 15
              0011 = 3
              0101 = 5
                        0011    (3)
                        0101    (5)
                        ____*
                        0011
                       0000
                      0011
                     0000
                     _______
                   0000 1111 = 15
    
    Proses Pembagian

    15 / 3 = 5 
                              ¹⁰¹   nilai Hasil 
                              ____  
                   (3)   0011/1111  (15)
                              11 
                              ____-
                               011
                                11
                              ____
                                00
==================================================================================================
    Konversi bilangan Hexadesimal

        Heksadesimal atau sistem bilangan basis 16 adalah sebuah sistem bilangan yang menggunakan 16 simbol
        Berbeda dengan sistem bilangan desimal, simbol yang digunakan dari sistem ini adalah angka 0 sampai 9
        ditambah dengan 6 simbol lainnya dengan menggunakan huruf A hingga F
            0 1 2 3 4 5 6 7 8 9 A B C D E F
        
        Teori dasar Hexadesimal
            Desimal = 323
            n*16³   n*16²   n*16¹   n*16⁰


            n*4096  n*256   n*16    n*1 (dan seterusnya)


            mencari perkalian yang mendekati angka soal 323
            mulai dari yang terbesar hingga angka kecil

            
            n*4096  n*256   n*16    n*1
            
            N = 1 # mencari perkalian yang mendekati angka 323
            1 * 256 = 256 # masih lebih kecil dari angka 323

                melakukan pengurangan Angka dengan hasil perkalian yang ditemukan
                3 2 3
                2 5 6
                _____-
                67


            N = 4 # mencari perkalian yang mendekati angka 67 yang telah dikurangi
            4 * 16 = 64 # masih lebih kecil dari angka 67

                melakukan pengurangan Angka dengan hasil perkalian yang ditemukan
                3 2 3
                2 5 6
                _____-
                6 7
                6 4
                ___-
                3
                
                
            N = 3 # mencari perkalian yang mendekati angka 3 yang telah dikurangi
            3 * 1 = 3 # angka ini telah habis dan kita telah menemukan angka  dari desisam 323
                3 2 3
                2 5 6
                _____-
                  6 7
                  6 4
                _____-
                    3
                    3
                _____-
                    0
            Angka yang ditemukan N adalah jawabanya
            1       4      3

            143 adalah bentuk Hexadesimal dari 323 desimal


            Desimal = 177
            hexa    = ???


            n*4096  n*256   n*16    n*1

            n = 11 # mencari perkalian yang mendekati angka soal 177
            11 * 16 = 176 # hampir mendekati angka soal 177
                1 7 7
                1 7 6
                _____-
                    1

            n = 1 # mencari perkalian yang mendekati angka soal 1
            1 * 1 = 1 # nilai habis, operasi selesai
                1 7 7
                1 7 6
                _____-
                    1
                    1
                _____-
                    0 
            
            Angka yang ditemukan N adalah jawabanya        
            11  1 
            ↓
            angka ini harus dirubah menjadi nilai 11 dari simbol berikut 0 1 2 3 4 5 6 7 8 9 A B C D E F
            11 = B
            Maka hasil 177 Desimal dikonversi ke Hexadesimal adalah B1

            Desimal = 177
            hexa    = B1

            
            
    Konversi bilangan Hexadesimal ke Desimal

        Hexadesimal = 64
        Desimal     = ???

            6 4 harus dipisah dan dikalikan dengan angka basis 16 dan dimulai dari kanan

            4 * 16⁰ = 4

            6 * 16¹ = 96

            4 + 96 = 100

            100 adalah hasil dari 64 Hexadesimal
==================================================================================================

bit adalah digit dari bilangan biner yang dikelompokan
menjadi 8 bit untuk mewakili 1 karakter
		 
		      8 digit
		         ↑					
	       : 0100 0001 ( bahasa mesin )
	       : 0100 0001 = 65 ( desimal)
	       : 0100 0001 = A ( dikonversi ke dalam bahasa manusia )
	       : 0100 0001 = 8 bit
	       : 8 bit 	   = 1 byte
	       
==================================================================================================
Konversi bilangan Oktal

Oktal atau sistem bilangan basis 8 adalah sebuah sistem bilangan berbasis delapan.
Simbol yang digunakan pada sistem ini adalah 0,1,2,3,4,5,6,7.
Konversi Sistem Bilangan Oktal berasal dari Sistem bilangan biner
yang dikelompokkan tiap tiga bit biner dari ujung paling kanan (LSB atau Least Significant Bit).

    Biner       Oktal   Desimal
    000 000	    00  	0
    000 001	    01  	1
    000 010	    02  	2
    000 011	    03  	3
    000 100	    04  	4
    000 101	    05  	5
    000 110	    06  	6
    000 111	    07  	7
    001 000	    10  	8
    001 001	    11  	9
    001 010	    12  	10
    001 011	    13  	11
    001 100	    14  	12
    001 101	    15  	13
    001 110	    16  	14
    001 111	    17  	15

	Teori dasar Oktal

		n*8³ 	n*8²	n*8¹	n*8°
		=	=	=	=		     	
		n*512	n*64	n*8	n*1

Contoh : Desimal = 323
       : Oktal 	 = ???

	
		 	 Diketahui desimal 323
		         Ditanya berapa Oktal

				Mencari nilai yang mendekati angka 323 (tidak boleh lebih)
				
				n*64 
				Jika n = 5
				maka 5 * 64 = 320 (mendekati angka 323)
				
				melakukan pengurangan dari hasil 5 * 64 dengan angka yang diketahui 323 (desimal)
			
				3 2 3
				3 2 0
				_____-
				3
				
					Diketahui desimal 3 (hasil dari pengurangan 323 - 320 = 3)
					Ditanya berapa Oktal
				
					n*8
					Jika n = 0
					maka 0 * 8 = 0 (mendekati angka 3)

					melakukan pengurangan dari hasil 0 * 8 dengan angka yang diketahui 3 (desimal)
				
					3
					0
					__-
					3

				
						n*1
						Jika n = 3
						maka 3 * 1 = 3 (sudah menemukan angka habis)
			
						melakukan pengurangan dari hasil 3 * 1 dengan angka yang diketahui 3 (desimal)

						3
						3
						__-
						0

	Nilai n = 5 0 3
		  Nilai 5 0 3 adalah nilai Oktal
		  desimal 323
==================================================================================================
LSB atau Least Significant Bit

Least significant bit adalah bagian dari barisan data biner
yang mempunyai nilai paling tidak berarti/paling kecil. 
Letaknya adalah paling kanan dari barisan bit. 
Sedangkan most significant bit adalah sebaliknya,
yaitu angka yang paling berarti/paling besar dan
letaknya disebelah paling kiri.

Least significant bit sering kali digunakan untuk kepentingan penyisipan data
ke dalam suatu media digital lain, salah satu yang memanfaatkan Least significant bit
sebagai metode penyembunyian adalah steganografi audio.
    

    MSB juga bisa berarti "byte paling signifikan". Artinya sejajar dengan yang di atas: byte
    dalam posisi bilangan multi-byte itulah yang memiliki nilai potensial terbesar. Untuk menghindari ambiguitas ini,
    istilah yang kurang disingkat "MSbit" atau "MSbyte" sering digunakan.