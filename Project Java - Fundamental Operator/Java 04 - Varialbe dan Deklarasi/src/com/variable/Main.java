
public class Main {

    public static void main(String[] args){
        System.out.println("Hello Variable dan Deklarasi");

        // Deklarasi variabel
      int nilaiInt;
      double sisiLebar;
      boolean nilaiBenar;
      char nilaiKarakter;
	  
      // Memberi nilai ke variabel
      nilaiInt = 10;
      sisiLebar = 5.4;
      nilaiBenar = true;
      nilaiKarakter = 'B';

      System.out.println(nilaiInt);
      System.out.print(sisiLebar);
      System.out.println(nilaiBenar);
      System.out.println(nilaiKarakter);

    }
}

