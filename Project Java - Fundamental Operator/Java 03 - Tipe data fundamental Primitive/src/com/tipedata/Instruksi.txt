Variabel:   
            (Lat) 1. berubah-ubah, tidak tetap; 2. deklarasi sesuatu yang memiliki variasi nilai 3.
            Berbeda-beda dalam bahasa pemrograman disebut juga simbol yang mewakili nilai tertentu,
            variabel yang dikenal di sub program disebut variabel lokal. sedang yang di kenal secara
            umum/utuh dalam satu program disebut variabel global.

    Macam-macam tipe data

        char: Tipe data karakter, contoh Z
        int: angka atau bilangan bulat, contoh 29
        float: bilangan desimal, contoh 2.1
        double: bilangan desimal juga, tapi lebih besar kapasistanya, contoh 2.1
        String: kumpulan dari karakter yang membentuk teks, contoh Hello World!
        boolean: tipe data yang hanya bernilai true dan false

        Tipe data Primitive 
            Dinamakan data primitive karena nilai min dan max sudah ditentukan
