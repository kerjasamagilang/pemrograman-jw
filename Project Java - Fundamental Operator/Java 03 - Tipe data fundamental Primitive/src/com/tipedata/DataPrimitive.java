
public class DataPrimitive {
    public static void main(String[] args){


        // Tipe data di Java :
        // integer, byte, short, long, double, float, char, boolean

        // INTEGER
        // Satuan
        int i = 10;
        
        System.out.println("======INTEGER======");
        System.out.println("Nilai Integer = "+ i);
        System.out.println("Nilai Max = " + Integer.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Integer.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Integer = " + Integer.BYTES + "Bytes"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Integer = " + Integer.SIZE + "Bit"); //  Besar ukuran memori dalam bit 


        // Byte
        // Satuan
        byte b = 10;
        
        System.out.println("======BYTE======");
        System.out.println("Nilai Byte = " + b);
        System.out.println("Nilai Max = " + Byte.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Byte.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Byte = " + Byte.BYTES + "Bytes"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Byte = " + Byte.SIZE + "Bit"); //  Besar ukuran memori dalam bit 
        

        // SHORT
        // Satuan
        short s  = 10;
        
        System.out.println("======SHORT======");
        System.out.println("Nilai SHORT = " + s);
        System.out.println("Nilai Max = " + Short.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Short.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Short = " + Short.BYTES + "Byte"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Short = " + Short.SIZE + "Bit"); //  Besar ukuran memori dalam bit


        // LONG 
        // Satuan 
        long l = 10;
        
        System.out.println("======LONG======");
        System.out.println("Nilai LONG = " + l);
        System.out.println("Nilai Max = " + Long.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Long.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Long = " + Long.BYTES + "Byte"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Long = " + Long.SIZE + "Bit"); //  Besar ukuran memori dalam bit

        // DOUBLE
        // Koma, Bilangan real
        double d = 10;
        
        System.out.println("======DOUBLE======");
        System.out.println("Nilai Double = " + d);
        System.out.println("Nilai Max = " + Double.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Double.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Byte = " + Double.BYTES + "Bytes"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Byte = " + Double.SIZE + "Bit"); //  Besar ukuran memori dalam bit


        // FLOAT
        // Koma, bilangan real
        float f = 10;
        
        System.out.println("======FLOAT======");
        System.out.println("Nilai Float = " + f);
        System.out.println("Nilai Max = " + Byte.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Byte.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Float = " + Byte.BYTES + "Bytes"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Float = " + Byte.SIZE + "Bit"); //  Besar ukuran memori dalam bit

        // Char
        // Koma, Bilangan real #Berdasarkan ASCII
        char c = 'H';
        
        System.out.println("======CHART======");
        System.out.println("Nilai Char = " + c);
        System.out.println("Nilai Max = " + Character.MAX_VALUE); //Menggunakan Hyperclass
        System.out.println("Nilai Min = " + Character.MIN_VALUE); //Mengguna Hyperclass
        System.out.println("Besar Byte = " + Character.BYTES + "Bytes"); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Byte = " + Character.SIZE + "Bit"); //  Besar ukuran memori dalam bit
        
        // BOOLEAN
        // TRUE atau FALSE 
        boolean bb = true;
        
        System.out.println("======BOOLEAN======");
        System.out.println("Nilai Boolean = " + bb);
        System.out.println("Besar Byte = " + Boolean.TRUE); // Besar ukuran memori dalam Bytes
        System.out.println("Besar Byte = " + Boolean.FALSE); //  Besar ukuran memori dalam bit
    }


}
