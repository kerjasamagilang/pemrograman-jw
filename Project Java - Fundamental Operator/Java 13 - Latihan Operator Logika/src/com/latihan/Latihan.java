package com.latihan;

import java.util.*;

public class Latihan {
    public static void main(String[] args){
        System.out.println("Latihan Operator Logika");

        // Membuat sebuah objek untuk menangkap input dari user
        Scanner inputUser = new Scanner(System.in);

        // Program sederhana untuk menebak sebuah angka
        int nilaiBenar = 6;
        int nilaiTebakan;
        boolean statusTebakn;

        System.out.println("Masukan nilai tebakan anda: ");
        nilaiTebakan = inputUser.nextInt();
        System.out.println("Nilai tebakan anda adalah: " + nilaiTebakan);


        // operasi Logika
        statusTebakn = (nilaiTebakan == nilaiBenar);
        System.out.println("Nilai tebakan anda: " + statusTebakn);

        // Operasi Aljabar boolean (or / and)
        System.out.println("Masukan nilai diantara 4 dan 9");
        nilaiTebakan = inputUser.nextInt();
        statusTebakn = (nilaiTebakan > 4) && (nilaiTebakan < 9);
        System.out.println("Nilai tebakan anda: " + statusTebakn);
        
        inputUser.close(); // Close inputUser
        
    }
}
