package com.scanner;

import java.util.Scanner;

public class ScannerTest { // Nama class tidak boleh Scanner
    public static void main(String[] args) {
        // deklarasi
        int luas;
        int alas;
		int tinggi;

        // mebuat scanner baru
        Scanner baca = new Scanner(System.in);

        // Input
        System.out.println("== Program hitung luas Segitiga ==");
        System.out.print("Input alas: ");
        alas = baca.nextInt();
        System.out.print("Input tinggi: ");
        tinggi = baca.nextInt();

        // proses
        luas = ((alas * tinggi) / 2);

        // output
        System.out.println("Luas = " + luas);
    }
}
