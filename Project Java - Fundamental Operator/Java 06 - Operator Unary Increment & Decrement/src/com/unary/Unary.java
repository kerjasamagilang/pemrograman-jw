package com.unary;

public class Unary {
    public static void main(String[] args){
        System.out.println("Hello Unary");

        // Unary = Operasi yang dilakukan pada satu variable
        // operasi unary adalah operasi dengan hanya satu operand,
        // yaitu satu input. Ini berbeda dengan operasi biner, yang menggunakan dua operan
        // operator unary penting banget sebelum masuk looping
        
        // Unary + dan -
        int angka = 1;
        System.out.printf("Unary '+' %d menjadi %d \n ",angka, +angka); 
        System.out.printf("Unary '-' %d menjadi %d \n ",angka, -angka); 

        // Unary Increment dan Decrement
        int angka2 = 10;
        angka2++; // menambahkan 1 nilai
        System.out.println("Nilai dengan '++' menjadi = " + angka2); // unary Increment = Pengingkatan

        int angka3 = 10;
        angka3--; // mengurangi 1 nilai
        System.out.println("Nilai dengan '--' menjadi = " + angka3); // unary Decrement = Pengurangan

        // prefix dan postfix
        int a = 10;
        System.out.printf("Nilai dengan '++' prefix menjadi = %d \n", ++a); // Jika ++ ditaruh di belakang data maka akan ditambah terlebih dahulu lalu ditampilkan
        int b = 10;
        System.out.printf("Nilai dengan ++ postfix menjadi = %d \n", b++); // jika ++ ditaruh di sesudah data makan data akan ditampilak terlebih dahulu

        // Unary boolean dengan ! untuk negasi
        boolean gilang = true;
        System.out.println("Nilai boolean = " + gilang);
        System.out.println("Nilai boolean = " + !gilang);

    }
}
