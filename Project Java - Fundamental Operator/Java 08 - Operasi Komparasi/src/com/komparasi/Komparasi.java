package com.komparasi;
public class Komparasi {
    public static void main(String[] args){
        System.out.println("Hello Komparasi \n");

        // Operasi Komparasi ... ini akan menghasilkan nilai dalam bentuk boolean
        
        // operator equal atau persamaan
        System.out.println("==========PERSAMAAN==========\n");
        int a,b;
        a = 10;
        b = 10;

        
        System.out.printf("%d apakah sama dengan %d ==> %s \n", a, b, (a == b));

        int c,d;
        c = 10;
        d = 11;
        System.out.printf("%d apakah sama dengan %d ==> %s \n", c, d, (c == d));

        int e,f;
        boolean hasilKomparasi;
        e = 10;
        f = 15;

        hasilKomparasi = (e == f);
        System.out.printf("%d Apakah sama dengan %d ==> %s \n", e, f, hasilKomparasi + "\n");

        // operator not equal atau pertidakpesamaan
        System.out.println("==========PERTIDAKPERSAMAAN==========\n");
        int m,n;
        m = 10;
        n = 10;

        
        System.out.printf("%d != %d ==> %s \n", m, n, (m != n));

        int o,p;
        o = 10;
        p = 11;
        System.out.printf("%d != %d ==> %s \n", o, p, (o != p));

        int r,s;
        boolean hasilKomparasiA;
        r = 10;
        s = 15;

        hasilKomparasiA = (r != s);
        System.out.printf("%d != %d ==> %s \n", r, s, hasilKomparasiA);
        

        // operator less atau kurang dari
        System.out.println("==========KURANG DARI==========\n");
        int g,h;
        g = 10;
        h = 10;

        
        System.out.printf("%d < %d ==> %s \n", g, h, (g < h));

        int i,j;
        i = 10;
        j = 11;
        System.out.printf("%d < %d ==> %s \n", i, j, (i < j));

        int k,l;
        boolean hasilKomparasiB;
        k = 10;
        l = 15;

        hasilKomparasiB = (k < l);
        System.out.printf("%d < %d ==> %s \n", k, l, hasilKomparasiB);


        // operator greater than atau lebih dari 
        System.out.println("==========LEBIH DARI==========\n");
        int ab,cd;
        ab = 10;
        cd = 10;

        
        System.out.printf("%d > %d ==> %s \n", ab, cd, (ab > cd));

        int ef,gh;
        ef = 10;
        gh = 11;
        System.out.printf("%d > %d ==> %s \n", ef, gh, (ef > gh));

        int ij,kl;
        boolean hasilKomparasiABC;
        ij = 10;
        kl = 15;

        hasilKomparasiABC = (ij > kl);
        System.out.printf("%d > %d ==> %s \n", ij, kl, hasilKomparasiABC);

    }
}
