package com.assignment;
public class Assignment{
    public static void main(String[] args){
        System.out.println("Hello Assignment");


        // Operator Assignment
        
        // dasar
        int a = 10;

        a = a + 10;
        System.out.println("Nilai a = " + a);

        // UMUM
        // Operator Assignment penjumlahan
        int b = 10;
        b += 10; // b = b + 10;
        System.out.println("Nilai b = " + b);

        // Operator Assignment pengurangan
        int c = 100;
        c -= 25; 
        System.out.println("Nilai c = " + c);

        // Operator Assignment pembagian
        int d = 50;
        d /= 4;
        System.out.println("Nilai d = " + d);

        // Operator Assignment perkalian
        int e = 9;
        e *= 9;
        System.out.println("Nilai e = " + e);

        // Operator Assignment modulus
        int f = 12;
        f %= 7; 
        System.out.println("Nilai f = " + f);
    }
}