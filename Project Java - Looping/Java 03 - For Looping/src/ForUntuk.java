public class ForUntuk{
    public static void main(String[] args) {
        System.out.println("Hello, ini adalah for loop");
        // for (inisialisasi; kondisi; step nilai) {
        //     aksi;
        // }
        
        System.out.println("Ini adalah program awal");

        for(int nilai = 0; nilai <= 10; nilai++){
            System.out.println("For loop ke ->" + nilai);
        }
    }
}