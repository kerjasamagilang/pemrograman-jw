public class UlanganDoWhile {
    public static void main(String[] args){
        System.out.println("Hello, ini adalah perulangan do while");

        // Deklarasi 
        int a = 0;
        boolean kondisi = true;

        do {
            a++;
            System.out.println("Ini adalah program awal ke->" + a); // Aksi
                if (a == 10){ // perulangan dilakukan 10 kali
                    kondisi = false;
                }
        } while (kondisi); // Kondisi 

        System.out.println("Program Selsai");
    }
}

// do while melakukan perulangan sudah aksi
// Mulai -> Lakukan Sesuatu ->  Ulangi -> Kondisi akhir -> Selesai
                            //  Ulangi jika kondisi true