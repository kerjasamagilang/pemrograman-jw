public class While {
    public static void main(String[] args){
        System.out.println("Hello, ini adalah Looping sederhana");

        // while (kondisi){
        //     aksi
        // }

        int a = 0;
        boolean kondisi = true;

        System.out.println("Awal program");
        while (kondisi){
            System.out.println("While Looping" + a);
            a++;

            if (a == 10 ){
                kondisi = false;
            }
        }
        System.out.println("Akhir program");
    }
}
