public class Kontrol{
    public static void main(String[] args) {
        
        // break, continue, dan return
        // Control Flow
        int a = 0;

        while (true){
            a++;

            if (a == 10){
                System.out.println("Ini adalah akhir");
                break;
                // ini adalah keyword untuk memaksa keluar dari loop
            } else if (a == 5) {
                continue;
                // ini adalah keyword untuk memaksa aksi dari awal
            } else if (a == 7){
                return;
            }

            System.out.println("Ke - " + a);
            
        }
    }
}