package com.latihan;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args){
        System.out.println("Hello, ini latihan Kalkulator");

        // Deklarasi
        Scanner inputUser;
        float a, b, hasil;
        char operator;
        

        inputUser = new Scanner(System.in);
        System.out.print("Masukan Nilai A = ");
        a = inputUser.nextFloat();

        System.out.print("Masukan Operator = ");
        operator = inputUser.next().charAt(0);

        System.out.print("Masukan Nilai A = ");
        b = inputUser.nextFloat();

        System.out.println("Input user " + a + " " + operator + " " + b);


        // Operator yang tersedia * / + -

        if (operator == '+') {
            hasil = a + b;
            System.out.println("hasil = " + hasil);
            

        } else if (operator == '-') {
            hasil = a - b;
            System.out.println("hasil = " + hasil);
        } else if (operator == '/') {
            hasil = a / b;
            if (b == 0){
                System.out.println("Pembagi nol menghasilkan tak terdefinisi"); 
            }else{
                System.out.println("hasil = " + hasil);
            }
        } else if (operator == '*') {
            hasil = a * b;
            System.out.println("hasil = " + hasil);
        } else {
            System.out.println("Operator tidak ditemukan");
        }

        inputUser.close();
    }
}
