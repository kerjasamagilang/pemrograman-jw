package com.kalkulator;

import java.util.Scanner;



public class SwitchCase {
    public static void main(String[] args){
        System.out.println("Hello, ini adalah Kalkulator dengan Switch Case");


        // Deklarasi
        Scanner inputUser;
        float a, b, hasil;
        String operator;

        inputUser = new Scanner(System.in);
        
        System.out.print("Nilai a = ");
        a = inputUser.nextFloat();
        System.out.print("operator = ");
        operator = inputUser.next();
        System.out.print("Nilai b = ");
        b = inputUser.nextFloat();

        switch (operator) {
            case "+":
            hasil = a + b;
                System.out.println("penjumlahan " + hasil);
                break;
            case "-":
            hasil = a - b;
                System.out.println("Pengurangan " + hasil);
                break;
            case "*":
            hasil = a * b;
                System.out.println("Perkalian " + hasil);
                break;

            case "/":
            hasil = a / b;
                System.out.println("Pembagian " + hasil);
                break;
        
            default:
                System.out.println("operator tidak ditemukan ");
                break;
        }
    }
}