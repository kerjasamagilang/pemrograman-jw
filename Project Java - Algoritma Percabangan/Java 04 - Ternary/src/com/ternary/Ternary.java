package com.ternary;
import java.util.Scanner;

public class Ternary {
    public static void main(String[] args){
        System.out.println("Hello, ini adalah operasi ternary");
        
        // operator ternary adalah operator yang mengambil tiga argumen
        
        
        int input, x;
        Scanner inputUser = new Scanner(System.in);
        System.out.print("Masukan angka");
        input = inputUser.nextInt();
        System.out.println(input);
        
        
        // variable x = eskpresi ? statement_true : statement_false
        x = (input <= 10 ) ? (input*input) : (input/2);
        System.out.println("Hasilnya = " + x);

        // Jika memakai if else
        // if (input <= 10 ) {
        //     x = input*input;
        // } else {
        //     x = input/2;
        // }

    }
}
