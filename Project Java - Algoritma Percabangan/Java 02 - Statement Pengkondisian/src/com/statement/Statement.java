package com.statement;

import java.util.Scanner;

public class Statement {
    public static void main(String[] args){
        System.out.println("Hello Statement Pengkondisian");
        
        // if else if statement


        // Deklarasi
        int angka;
        System.out.println("Ini adalah program awal");

        // input
        Scanner userInput = new Scanner(System.in);

        System.out.println(" masukan angka ");
        angka = userInput.nextInt();

        // Statement
        if (angka <= 5) { // Jika a lebih kecil dari sama dengan 5 
            System.out.println("Nomor yang kamu pilih dibawah 10"); // lakukan sesuatu
            
        } 
        else if (angka >= 10) { // selain itu jika a lebih besar dari sama dengan 10 
            System.out.println("Angka yang kamu pilih lebih dari 10"); // Lakukan sesuatu

        }
        else { // selain itu lakukan ini
            System.out.println("Nomor yang kamu pilih diantara 5 dan 10");
        }
    }
}