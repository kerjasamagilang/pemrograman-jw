package com.pengkondisian;

import java.util.Scanner;

public class Condition {
    public static void main(String[] args){
        System.out.println("Hello Condition");

        // Deklarasi 
        boolean benar;
        boolean salah;
        int angka;

        // Membuat Scanner baru
        Scanner userInput = new Scanner(System.in);

        // Input 
        System.out.println("Program Ganjil Genap");
        System.out.println("Masukan Nomor ");
        angka = userInput.nextInt();

        // Proses
        benar = true;
        salah = false;
        if (angka % 2 == 0) {
            System.out.println(angka + " adalah bilangan genap");
        } else {
            System.out.println(angka + " adalah bilangan ganjil");
        }
    }
}