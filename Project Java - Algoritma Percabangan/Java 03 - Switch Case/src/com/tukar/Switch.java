package com.tukar;

import java.util.*;

public class Switch {
    public static void main(String[] args){
        System.out.println("Hello ini adalah Program Switch Case");

        String input;

        Scanner inputUser = new Scanner(System.in);
        
        System.out.print("Panggina saya: ");
        input = inputUser.next();
        System.out.println(input);


        // Aksinya berupa satuan (int, long, byte, short), String atau enum
        
        switch (input){
            case "Gilang":
                System.out.println("Saya Gilang, hadir pak!!!");
                break; // Memberhentikan 
            default: // Default sama dengan else
            System.out.println(input + " Tidak hadir pak!!!");
        }
    }    
}
